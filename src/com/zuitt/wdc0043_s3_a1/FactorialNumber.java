package com.zuitt.wdc0043_s3_a1;

import java.util.Scanner;

public class FactorialNumber {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.println("Input an integer whose factorial will be computed");


        try {
            int num = in.nextInt();

            if (num >= 0) {
                int numCopy = num;
                int result = 1;
                for (int i = numCopy; i > 0; i--) {
                    result *= numCopy;
                    numCopy--;

                }
                System.out.println("The factorial of " + num + " is " + result);
            } else {
                System.out.println("Invalid input. Please enter a number greater than 0");
            }

        } catch (Exception e) {
            System.out.println("Invalid input. Please enter a number.");
            e.printStackTrace();

        }
    }

}
